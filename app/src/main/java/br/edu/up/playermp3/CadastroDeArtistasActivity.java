package br.edu.up.playermp3;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import br.edu.up.playermp3.dominio.Artista;

public class CadastroDeArtistasActivity extends AppCompatActivity {

    private Artista artista;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_artistas);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        Intent in = getIntent();
        artista = (Artista) in.getSerializableExtra("artista");

        TextView textView = (TextView) findViewById(R.id.txtNome);
        textView.setText(artista.getNome());
    }

    public void gravar(View view) {

        TextView textView = (TextView) findViewById(R.id.txtNome);
        String nome = textView.getText().toString();

        if (artista.getId() > 0){
            String[] valores = {nome};
            db.execSQL("UPDATE artistas SET nome = ? WHERE id = " + artista.getId(), valores);
        } else {
            db.execSQL("INSERT INTO artistas (nome) VALUES (?)", new String[]{nome});
        }

        Intent in = new Intent(this, ListaDeArtistasActivity.class);
        startActivity(in);
    }
}
