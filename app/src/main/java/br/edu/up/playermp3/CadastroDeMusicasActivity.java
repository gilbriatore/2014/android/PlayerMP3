package br.edu.up.playermp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.up.playermp3.dominio.Artista;
import br.edu.up.playermp3.dominio.Musica;

public class CadastroDeMusicasActivity extends AppCompatActivity {

    private Artista artista;
    private Musica musica;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_musicas);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        Intent in = getIntent();
        artista = (Artista) in.getSerializableExtra("artista");
        musica = (Musica) in.getSerializableExtra("musica");

        TextView textView = (TextView) findViewById(R.id.txtArtista);
        textView.setText(artista.getNome());

        TextView txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        TextView txtAutor = (TextView) findViewById(R.id.txtAutor);
        TextView txtAlbum = (TextView) findViewById(R.id.txtAlbum);
        txtTitulo.setText(musica.getTitulo());
        txtAutor.setText(musica.getAutor());
        txtAlbum.setText(musica.getAlbum());
    }

    public void gravar(View view) {

        TextView txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        TextView txtAutor = (TextView) findViewById(R.id.txtAutor);
        TextView txtAlbum = (TextView) findViewById(R.id.txtAlbum);
        String tituloStr = txtTitulo.getText().toString();
        String autorStr = txtAutor.getText().toString();
        String albumStr = txtAlbum.getText().toString();

        if (musica.getId() > 0){
            String[] valores = {tituloStr, autorStr, albumStr};
            db.execSQL("UPDATE musicas SET titulo = ?, autor = ?, album = ?, mp3 = ? WHERE id = " + musica.getId(), valores);
        } else {
            String[] valores = {String.valueOf(artista.getId()), tituloStr, autorStr, albumStr};
            db.execSQL("INSERT INTO musicas (id_artista, titulo, autor, album, mp3) values (?,?,?,?,?)", valores);
        }

        String[] params =  new String[]{String.valueOf(artista.getId())};
        Cursor resultado = db.rawQuery("SELECT * FROM musicas WHERE id_artista = ?", params);

        ArrayList<Musica> listaDeMusicas = new ArrayList<>();
        while(resultado.moveToNext()){
            int idMusica = resultado.getInt(resultado.getColumnIndex("id"));
            String titulo = resultado.getString(resultado.getColumnIndex("titulo"));
            String autor = resultado.getString(resultado.getColumnIndex("autor"));
            String album = resultado.getString(resultado.getColumnIndex("album"));
            String mp3 = resultado.getString(resultado.getColumnIndex("mp3"));

            Musica musica = new Musica(idMusica, artista.getId(), titulo, autor, album, mp3);
            listaDeMusicas.add(musica);
        }

        Intent in = new Intent(this, ListaDeMusicasActivity.class);
        in.putExtra("artista", artista);
        in.putExtra("listaDeMusicas", listaDeMusicas);
        startActivity(in);
    }
}