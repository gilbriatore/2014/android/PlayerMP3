package br.edu.up.playermp3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.edu.up.playermp3.dominio.Artista;
import br.edu.up.playermp3.dominio.Musica;

public class ListaDeArtistasActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private ArrayList<String> listaDeNomes;
    private ArrayList<Artista> listaDeArtistas;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_artistas);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        //db.execSQL("DROP TABLE artistas");

        db.execSQL("CREATE TABLE IF NOT EXISTS artistas (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, idGoogle TEXT)");

        //db.execSQL("DROP TABLE musicas");

        db.execSQL("CREATE TABLE IF NOT EXISTS musicas (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "id_artista INTEGER, titulo TEXT,  autor TEXT, album TEXT, mp3 TEXT)");

        listaDeArtistas = new ArrayList<>();
        Cursor resultado = db.rawQuery("select * from artistas",null);
        listaDeNomes = new ArrayList<>();
        while(resultado.moveToNext()){
            int id = resultado.getInt(resultado.getColumnIndex("id"));
            String nome = resultado.getString(resultado.getColumnIndex("nome"));
            Artista artista = new Artista(id, nome);
            listaDeArtistas.add(artista);
            listaDeNomes.add(nome);
        }

        adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, listaDeNomes);
        ListView listView = (ListView) findViewById(R.id.lista_de_artistas);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artista artista = listaDeArtistas.get(position);
                int idPessoa = artista.getId();

                String url = "http://player-mp3.appspot.com/ws/musicas/" + artista.getIdGoogle();
                StringRequest request = new StringRequest(url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String json) {
                        sincronizarLista(json);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });


                String[] params = new String[]{String.valueOf(idPessoa)};

                Cursor resultado = db.rawQuery("SELECT * FROM musicas WHERE id_artista = ?", params);

                ArrayList<Musica> listaDeMusicas = new ArrayList<>();
                while (resultado.moveToNext()) {
                    int idMusica = resultado.getInt(resultado.getColumnIndex("id"));
                    String titulo = resultado.getString(resultado.getColumnIndex("titulo"));
                    String autor = resultado.getString(resultado.getColumnIndex("autor"));
                    String album = resultado.getString(resultado.getColumnIndex("album"));
                    String mp3 = resultado.getString(resultado.getColumnIndex("mp3"));

                    Musica musica = new Musica(idMusica, idPessoa, titulo, autor, album, mp3);
                    listaDeMusicas.add(musica);
                }

                if (listaDeMusicas.size() > 0) {
                    Intent in = new Intent(ListaDeArtistasActivity.this, ListaDeMusicasActivity.class);
                    in.putExtra("artista", artista);
                    in.putExtra("listaDeMusicas", listaDeMusicas);
                    startActivity(in);
                } else {
                    Intent in = new Intent(ListaDeArtistasActivity.this, CadastroDeMusicasActivity.class);
                    in.putExtra("artista", artista);
                    in.putExtra("musica", new Musica());
                    startActivity(in);
                }
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final Artista artista = listaDeArtistas.get(position);
                final String artistaAdapter = listaDeNomes.get(position);

                String[] acoes = {"Alterar", "Excluir", "Cancelar"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaDeArtistasActivity.this);
                builder.setTitle("O que você deseja fazer?")
                        .setItems(acoes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        alterar(artista);
                                        break;
                                    case 1:
                                        excluir(artista, artistaAdapter);
                                        break;
                                }
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });
    }

    private void alterar(Artista artista){
        Intent in = new Intent(this, CadastroDeArtistasActivity.class);
        in.putExtra("artista", artista);
        startActivity(in);
    }

    private void excluir(final Artista artista, final String artistaAdapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tem certeza que deseja excluir?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Cursor resultado = db.rawQuery("SELECT id FROM musicas WHERE id_artista = ?", new String[]{String.valueOf(artista.getId())});
                if (resultado.getCount() > 0) {
                    Toast.makeText(ListaDeArtistasActivity.this, "Remova as músicas primeiro!", Toast.LENGTH_LONG).show();
                } else {
                    db.execSQL("DELETE FROM artistas WHERE id = " + artista.getId());
                    adapter.remove(artistaAdapter);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Não faz nada.
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_artistas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.incluir) {
            Intent in = new Intent(this, CadastroDeArtistasActivity.class);
            in.putExtra("artista", new Artista());
            startActivity(in);
        }

        if (id == R.id.sincronizar){
            String url = "http://player-mp3.appspot.com/ws/artistas";
            StringRequest request = new StringRequest(url, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {
                    sincronizarLista(json);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });

            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(request);
        }

        return true;
    }

    private void sincronizarLista(String json) {

        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i <= json.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String nome = jsonObject.getString("nome");
                String idGoogle = jsonObject.getString("id");

                db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);
                Cursor resultado = db.rawQuery("select * from artistas where nome = ?", new String[]{nome});
                if (!resultado.moveToNext()){
                    db.execSQL("INSERT INTO artistas (nome, idGoogle) VALUES (?,?)", new String[]{nome, idGoogle});
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
