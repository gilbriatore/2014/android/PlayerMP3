package br.edu.up.playermp3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.edu.up.playermp3.dominio.Artista;
import br.edu.up.playermp3.dominio.Musica;

public class ListaDeMusicasActivity extends AppCompatActivity {

    private Artista artista;
    private SQLiteDatabase db;
    private List<String[]> listaDeTitulos;
    private ArrayList<Musica> listaDeMusicas;
    private ArrayAdapter<String[]> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_musicas);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        Intent in = getIntent();
        artista = (Artista) in.getSerializableExtra("artista");
        listaDeMusicas = (ArrayList<Musica>) in.getSerializableExtra("listaDeMusicas");

        listaDeTitulos = converterListaDeMusicas(listaDeMusicas);

        adapter = criarAdapter(listaDeTitulos);
        ListView listView = (ListView) findViewById(R.id.lista_de_musicas);
        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Musica musica = listaDeMusicas.get(position);
                final String[] musicaAdapter = listaDeTitulos.get(position);

                String[] acoes = {"Alterar", "Excluir", "Cancelar"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaDeMusicasActivity.this);
                builder.setTitle("O que você deseja fazer?")
                        .setItems(acoes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        alterar(musica);
                                        break;
                                    case 1:
                                        excluir(musica, musicaAdapter);
                                        break;
                                }
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });
    }

    private List<String[]> converterListaDeMusicas(ArrayList<Musica> listaDeMusicas) {
        List<String[]> lista = new ArrayList();
        for (Musica musica : listaDeMusicas) {
            String[] strings = new String[]{musica.getTitulo(), musica.getAutor() + ", " + musica.getAlbum()};
            lista.add(strings);
        }
        return lista;
    }

    private ArrayAdapter<String[]> criarAdapter(final List<String[]> listaDeStrings) {

        ArrayAdapter<String[]> adapterPersonalizado = new ArrayAdapter<String[]>(
                this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                listaDeStrings) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                String[] vetor = listaDeStrings.get(position);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setText(vetor[0]);
                text2.setText(vetor[1]);
                return view;
            }
        };
        return adapterPersonalizado;
    }

    private void alterar(Musica musica) {
        Intent in = new Intent(this, CadastroDeMusicasActivity.class);
        in.putExtra("artista", artista);
        in.putExtra("musica", musica);
        startActivity(in);
    }

    private void excluir(final Musica musica, final String[] musicaAdapter) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tem certeza que deseja excluir?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                db.execSQL("DELETE FROM musicas WHERE id = " + musica.getId());
                String[] remove = new String[]{musica.getTitulo(), musica.getAutor() + ", " + musica.getAlbum()};
                adapter.remove(musicaAdapter);
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Não faz nada.
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_enderecos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.inicio) {
            Intent in = new Intent(this, ListaDeArtistasActivity.class);
            startActivity(in);
        }

        if (id == R.id.incluir) {
            Intent in = new Intent(this, CadastroDeMusicasActivity.class);
            in.putExtra("artista", artista);
            in.putExtra("musica", new Musica());
            startActivity(in);
        }

        if (id == R.id.sincronizar) {
            String url = "http://player-mp3.appspot.com/ws/musicas/" + artista.getIdGoogle();
            StringRequest request = new StringRequest(url, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {
                    sincronizarLista(json);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });

            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(request);
        }

        return true;
    }

    private void sincronizarLista(String json) {

        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i <= json.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String nome = jsonObject.getString("nome");

//                db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);
//                Cursor resultado = db.rawQuery("select * from artistas where nome = ?", new String[]{nome});
//                if (!resultado.moveToNext()) {
//                    db.execSQL("INSERT INTO artistas (nome) VALUES (?)", new String[]{nome});
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}