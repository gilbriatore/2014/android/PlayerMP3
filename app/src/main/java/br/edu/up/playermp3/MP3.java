package br.edu.up.playermp3;

import java.io.Serializable;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 16/09/2015
 */
public class MP3 implements Serializable {

    private String artista;
    private String musica;
    private String autor;
    private String album;

    public MP3(String artista, String musica, String autor, String album) {
        this.artista = artista;
        this.musica = musica;
        this.autor = autor;
        this.album = album;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getMusica() {
        return musica;
    }

    public void setMusica(String musica) {
        this.musica = musica;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
}