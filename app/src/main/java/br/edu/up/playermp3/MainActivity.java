package br.edu.up.playermp3;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
        String[] artistas = res.getStringArray(R.array.lista_de_artistas);
        List<String> lista = Arrays.asList(artistas);
        Collections.sort(lista);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_list_item_1,artistas);

        ListView lv = (ListView) findViewById(R.id.listaDeArtistas);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView txt = (TextView) view;
                String artista = txt.getText().toString();
                //Toast.makeText(MainActivity.this, artista, Toast.LENGTH_LONG).show();
                Intent in = new Intent(MainActivity.this, MusicasActivity.class);
                in.putExtra("artista", artista);
                startActivity(in);
            }
        });
    }
}
