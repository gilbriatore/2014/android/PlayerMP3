package br.edu.up.playermp3;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MusicasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musicas);

        Intent in = getIntent();
        final String artista = in.getStringExtra("artista");

        if (artista != null) {

            Resources res = getResources();
            int id = res.getIdentifier(artista.replace(" ", "_"), "array", this.getPackageName());

            if (id > 0) {

                String[] musicas = res.getStringArray(id);

                List<String[]> listaDeStrings = converterListaDeMusicas(musicas);
                ListAdapter adapter = criarAdapter(listaDeStrings);

                ListView lv = (ListView) findViewById(R.id.lista_de_musicas);
                lv.setAdapter(adapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        TextView txtMusica = (TextView) view.findViewById(android.R.id.text1);
                        String musica = txtMusica.getText().toString();
                        TextView txtQuebrar = (TextView) view.findViewById(android.R.id.text2);
                        String[] txts = txtQuebrar.getText().toString().split(",");
                        MP3 mp3 = new MP3(artista, musica, txts[0].trim(), txts[1].trim());
                        Intent in = new Intent(MusicasActivity.this, PlayerActivity.class);
                        in.putExtra("mp3", mp3);
                        startActivity(in);
                    }
                });
            }
        }
    }

    private List<String[]> converterListaDeMusicas(String[] musicas) {
        List<String[]> lista = new ArrayList();
        for(String str : musicas){
           lista.add(str.split(";"));
        }
        return lista;
    }

    @NonNull
    private ArrayAdapter<String[]> criarAdapter(final List<String[]> listaDeStrings) {

        ArrayAdapter<String[]> adapterPersonalizado = new ArrayAdapter<String[]>(
                this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                listaDeStrings) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                String[] vetor = listaDeStrings.get(position);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setText(vetor[0]);
                text2.setText(vetor[1]);
                return view;
            }
        };
        return adapterPersonalizado;
    }
}
