package br.edu.up.playermp3;

import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.edu.up.playermp3.dominio.Musica;

public class PlayerActivity extends AppCompatActivity {

    private Musica musica;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        Intent in = getIntent();
        musica = (Musica) in.getSerializableExtra("musica");

        //String nomeImagem = musica.getAlbum().toLowerCase().replace(" ", "_").trim();
        //Resources res = getResources();
        //int idImagem = res.getIdentifier(nomeImagem, "drawable", this.getPackageName());

        //ImageView iv = (ImageView) findViewById(R.id.imgAlbum);
        //iv.setImageResource(idImagem);

        TextView txtArtista = (TextView) findViewById(R.id.txtArtista);
        TextView txtMusica = (TextView) findViewById(R.id.txtMusica);
        TextView txtAutor = (TextView) findViewById(R.id.txtAutor);
        TextView txtAlbum = (TextView) findViewById(R.id.txtAlbum);

        //txtArtista.setText(musica.getArtista());
        txtMusica.setText(musica.getTitulo());
        txtAutor.setText(musica.getAutor());
        txtAlbum.setText(musica.getAlbum());
    }

    public void tocar(View view) {

        Button btn = (Button) view;

        if (mp == null) {
            String musica = this.musica.getTitulo().toLowerCase().replace(" ", "_");
            Resources res = getResources();
            int idMP3 = res.getIdentifier(musica, "raw", this.getPackageName());
            mp = MediaPlayer.create(this, idMP3);
            mp.start();
            btn.setText(R.string.btn_parar);
        } else {
            mp.stop();
            mp = null;
            btn.setText(R.string.btn_tocar);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mp != null){
            mp.stop();
            mp = null;
        }
    }
}
