package br.edu.up.playermp3.dominio;

import java.io.Serializable;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 15/10/2015
 */
public class Artista implements Serializable {

    private int id;
    private String nome;
    private String idGoogle;

    public Artista(){
    }

    public Artista(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdGoogle() {
        return null;
    }

    public void setIdGoogle(String idGoogle) {
        this.idGoogle = idGoogle;
    }
}
