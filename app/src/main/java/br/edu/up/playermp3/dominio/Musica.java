package br.edu.up.playermp3.dominio;

import java.io.Serializable;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 15/10/2015
 */
public class Musica implements Serializable {

    private int id;
    private int idArtista;
    private String titulo;
    private String autor;
    private String album;
    private String mp3;

    public Musica(){}

    public Musica(int id, int idArtista, String titulo, String autor, String album, String mp3) {
        this.id = id;
        this.idArtista = idArtista;
        this.titulo = titulo;
        this.autor = autor;
        this.album = album;
        this.mp3 = mp3;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getMp3() {
        return mp3;
    }

    public void setMp3(String mp3) {
        this.mp3 = mp3;
    }
}
